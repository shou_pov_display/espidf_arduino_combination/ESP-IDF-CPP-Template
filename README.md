# sen5x arduino 라이브러리를 ESP-IDF에서 쓸 수 있도록 한다.
**Quick Application Firmware Template to get your started**


- arduino-esp32는 esp-idf arduino 4.4 버전을 쓰고 있으며 용량이 커서 다른 폴더에서 받아서 하드 링크를 통해서 갖고 오고 있다.

```
jongju@jongju:~/project/shou/espidf-i2c-sen5x/components/arduino-esp32$ git branch --all
* idf-release/v4.4
  master
  remotes/origin/HEAD -> origin/master
  remotes/origin/SuGlider-patch-1
  remotes/origin/adding_more_libraries
  remotes/origin/espdocs
  remotes/origin/feature/network_refactoring
  remotes/origin/feature/rtos-cbuf
  remotes/origin/gh-pages
  remotes/origin/idf-release/v4.4
  remotes/origin/idf-release/v5.1
  remotes/origin/master
  remotes/origin/release/v1.0
  remotes/origin/release/v2.x
jongju@jongju:~/project/shou/espidf-i2c-sen5x/components/arduino-esp32$ git remote -v
origin	https://github.com/espressif/arduino-esp32.git (fetch)
origin	https://github.com/espressif/arduino-esp32.git (push)

jongju@jongju:~/project/shou/espidf-i2c-sen5x/components$ ls -al
합계 16
drwxrwxr-x 4 jongju jongju 4096 12월 14 14:00 .
drwxrwxr-x 8 jongju jongju 4096 12월 14 14:48 ..
-rw-rw-r-- 1 jongju jongju    0 12월 14 13:55 .gitkeep
drwxrwx--x 5 jongju jongju 4096 12월 14 14:06 arduino-core
lrwxrwxrwx 1 jongju jongju   20 12월 14 14:00 arduino-esp32 -> ../../arduino-esp32/
drwxrwx--x 5 jongju jongju 4096 12월 14 14:01 arduino-i2c-sen5x
```

## Detailed Steps below
 > 1. Clone espidf-i2c-sen5x `git clone https://gitlab.com/shou_pov_display/espidf_arduino_combination/espidf-i2c-sen5x`  
 > 2. Set device target `idf.py set-target esp32`
 > 3. Change firmware/project name in CMakeLists.txt file in the root : SKIP (선진행)   
  ```
# For more information about build system see
# https://docs.espressif.com/projects/esp-idf/en/latest/api-guides/build-system.html
# The following five lines of boilerplate have to be in your project's
# CMakeLists in this exact order for cmake to work correctly
cmake_minimum_required(VERSION 3.5)

include($ENV{IDF_PATH}/tools/cmake/project.cmake)
project(arduino-lib-idf)
 ```
 > 4. Create `components` folder `mkdir components && cd components`  : SKIP (선진행)
 > 5. `arduino-esp32`과  센시리온 `arduino-core`를 복사한다. : SKIP (선진행)

 ```
 git clone https://github.com/espressif/arduino-esp32.git arduino-esp32 : 이 부분은 용량 및 관리가 불편해서 로컬에 받은 것을 토대로 하드 링크로 대체한다. (idf release V4.4를 사용)
 https://github.com/Sensirion/arduino-core 에서 받아온다.
 ```  
 **IDF버전을 맞추기 위해 같이 최신 버전의 esp32라이브러리를 받은 뒤 git checkout으로 4.4q버전을 받아서 commit해 놓은 상태이다. **
 > 6. `arduino-i2c-sen5x_Library`를 components폴더에 복사한다. 폴더 이름은 `arduino-i2c-sen5x` : SKIP (선진행)
 ```
https://github.com/Sensirion/arduino-i2c-sen5x.git 에서 받아온다.
 ```  
> 7. Create CMakeLists.txt file inside the `arduino-i2c-sen5x` library folder   

```
# CMakeFiles.txt inside "SensirionI2CSen5x" folder
cmake_minimum_required(VERSION 3.5)
                    
idf_component_register(SRCS "src/SensirionI2CSen5x.cpp"
                      INCLUDE_DIRS "src" "../arduino-core/src"
                      REQUIRES arduino arduino-core
                      )
```
> 8. Update CMakeLists.txt file in the main folder   
```
# For more information about build system see
# https://docs.espressif.com/projects/esp-idf/en/latest/api-guides/build-system.html
# The following five lines of boilerplate have to be in your project's
# CMakeLists in this exact order for cmake to work correctly
cmake_minimum_required(VERSION 3.5)

include($ENV{IDF_PATH}/tools/cmake/project.cmake)
project(arduino-lib-idf)
```                    
 > 9. Run `idf.py menuconfig`  
 > 10. Build `idf.py build`  
 > 11. Flash `idf.py -p <serial-port> flash`  

## Folder contents
The project contains one source file in C++ language [main.cpp](main/main.cpp). The file is located in folder [main](main).

ESP-IDF projects are built using CMake. The project build configuration is contained in `CMakeLists.txt`
files that provide set of directives and instructions describing the project's source files and targets
(executable, library, or both). 

Below is short explanation of remaining files in the project folder.

```
├── CMakeLists.txt
├── components [step 4]
│   ├── arduino [submodules - step 5]
│   └── arduino-i2c-sen5x [submodules - step 6]
│       └── CMakeLists.txt [created in Step 7]
├── main
│   ├── CMakeLists.txt
│   └── main.cpp
└── README.md                  This is the file you are currently reading
```